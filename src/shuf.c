/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2022 Alessio Chiapperini
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 200112L
#elif _POSIX_C_SOURCE < 200112L
#  error incompatible _POSIX_C_SOURCE level
#endif

#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

static FILE *ofile = 0;

static int eflag = 0;	/* whether to treat each operand as an input line */
/*
 * whether to act as input came from a file containing
 * unsigned numbers in the range lo-hi, one per line
 */
static int iflag = 0;
static int oflag = 0;	/* whether to output lines on a specific file */
static int rflag = 0;	/* whether to repeat output lines */

static long count = -1;	/* argument of the -n flag */

static void
usage(void)
{
	(void)fprintf(stderr, "usage: shuf [-h] [-n count] [-o outfile] [-r]"
	    " [file]\n"
	    "       shuf [-h] -e [-n count] [-o outfile] [-r] [args ...]\n"
	    "       shuf [-h] -i lo-hi [-n count] [-o outfile] [-r]\n");
	exit(1);
}

static void
shuffle(char **text, long n)
{
	long i, j;
	char *tmp;

	for (i = n - 1; i > 0; i--){
		/*
		 * Could use better random generator and not introduce
		 * bias
		 */
		j = rand() % (i + 1);
		tmp = text[j];
		text[j] = text[i];
		text[i] = tmp;
	}

	n = count != -1 && count <= n ? count : n;
	for (i = 0; i < n; i++) {
		(void)fprintf(ofile, "%s\n", text[i]);
	}
}

static void
randomshuffle(char **text, long n)
{
	long i;

	while ((count == -1 ? 1 : count-- > 0)) {
		i = rand() % n;
		(void)fprintf(ofile, "%s\n", text[i]);
	}
}

static int
shufflefile(char *src)
{
	char buf[BUFSIZ];
	char **lines, **nlines;
	FILE *ifile;
	long entries, maxentries, i;
	int ret;

	ret = 0;
	ifile = stdin;
	if (src[0] != '-' || src[1] != '\0') {
		ifile = fopen(src, "r");
		if (ifile == 0) {
			ret = 1;
			perror("shuf");
			goto open_err;
		}
	}

	maxentries = BUFSIZ;
	lines = calloc((size_t)maxentries, sizeof *lines);
	if (lines == 0) {
		ret = 1;
		perror("shuf");
		goto lines_alloc_err;
	}

	for (i = 0; i < maxentries; i++) {
		lines[i] = calloc(BUFSIZ, 1);
		if (lines[i] == 0) {
			ret = 1;
			perror("shuf");
			goto line_alloc_err;
		}
	}

	entries = 0;
	while (fgets(buf, BUFSIZ, ifile) != 0) {
		buf[strcspn(buf, "\n")] = 0;

		if (entries == maxentries) {
			nlines = realloc(lines, (size_t)(maxentries + BUFSIZ) *
			    sizeof *nlines);
			if (nlines == 0) {
				ret = 1;
				perror("shuf");
				goto lines_realloc_err;
			}
			(void)memset(nlines + maxentries, 0, (sizeof *nlines) *
			    BUFSIZ);
			maxentries += BUFSIZ;
			lines = nlines;
		}
		(void)memcpy(lines[entries++], buf, BUFSIZ);
	}

	if (rflag) {
		randomshuffle(lines, entries);
	} else {
		shuffle(lines, entries);
	}

lines_realloc_err:
line_alloc_err:
	for (i = 0; i < maxentries; i++) {
		free(lines[i]);
	}
lines_alloc_err:
	free(lines);
	(void)fclose(ifile);
open_err:
	return (ret);
}

static int
shuffle_inrange(long lower, long higher)
{
	long range, i, j, tmp;
	long *interval;
	int ret;

	ret = 0;
	/* Check if it's safe to cast range to size_t */
	if (SIZE_MAX < LONG_MAX) {
		ret = 1;
		errno = ERANGE;
		perror("shuf");
		goto range_err;
	}

	range = higher - lower + 1;
	interval = calloc((size_t)range, sizeof(long));
	if (interval == 0) {
		perror("shuf");
		ret = 1;
		goto alloc_err;
	}

	for (i = 0; i < range; i++) {
		interval[i] = i + lower;
	}

	if (rflag) {
		while ((count == -1 ? 1 : count-- > 0)) {
			(void)fprintf(ofile, "%u\n", (unsigned)((rand() %
			    range) + lower));
		}
	} else {
		for (i = 0; i <= range - 2; i++){
			/*
			 * Could use better random generator and not introduce
			 * bias
			 */
			j = rand() % (range - i) + i;
			tmp = interval[i];
			interval[i] = interval[j];
			interval[j] = tmp;
		}

		range = count != -1 && count <= range ? count : range;
		for (i = 0; i < range; i++) {
			(void)fprintf(ofile, "%ld\n", interval[i]);
		}
	}

	free(interval);

alloc_err:
range_err:
	return (ret);
}

int
main(int argc, char *argv[])
{
	long lo, hi;	/* lower and upper bounds for the -i option */
	int opt;
	int ret;
	char *argp;
	char *def[] = { "-" };
	char *endptr;

	while ((opt = getopt(argc, argv, "ehi:n:o:r")) != -1) {
		switch (opt) {
		case 'e':
			eflag = 1;
			break;
		case 'h':
			usage();
			break;
		case 'i':
			iflag++;
			if (iflag > 1) {
				(void)fprintf(stderr, "shuf: cannot set the -i"
				    " flag multiple times\n");
				exit(1);
			}

			if ((argp = strchr(optarg, '-')) == 0) {
				(void)fprintf(stderr, "shuf: lower and upper"
				    " bounds must be provided for the -i"
				    " option\n");
				exit(1);
			}
			*argp = '\0';

			lo = strtol(optarg, &endptr, 10);
			if (optarg == endptr) {
				errno = EINVAL;
				perror("shuf");
				exit(1);
			} else if ((lo == LONG_MIN || lo == LONG_MAX) &&
			    errno == ERANGE) {
				perror("shuf");
				exit(1);
			}

			hi = strtol(argp + 1, &endptr, 10);
			if (optarg == endptr) {
				errno = EINVAL;
				perror("shuf");
				exit(1);
			} else if ((hi == LONG_MIN || hi == LONG_MAX) &&
			    errno == ERANGE) {
				perror("shuf");
				exit(1);
			}

			if (lo >= hi) {
				(void)fprintf(stderr, "shuf: lo must be less"
				    " than hi\n");
				exit(1);
			}

			if (lo == 0 && hi == INTMAX_MAX) {
				(void)fprintf(stderr, "lo-hi range too"
				    " large\n");
				exit(1);
			}

			break;
		case 'n':
			errno = 0;
			count = strtoimax(optarg, &endptr, 10);
			if (optarg == endptr) {
				errno = EINVAL;
				perror("shuf");
				exit(1);
			} else if ((count == INTMAX_MIN ||
			    count == INTMAX_MAX) && errno == ERANGE) {
				perror("shuf");
				exit(1);
			} else if (count < 0) {
				(void)fprintf(stderr, "shuf: -n count must be"
				    " from 0 to %jd, not %s\n", INTMAX_MAX,
				    optarg);
				exit(1);
			}
			break;
		case 'o':
			oflag++;
			if (oflag > 1) {
				(void)fprintf(stderr, "shuf: cannot have"
				    " multiple output files.\n");
				exit(1);
			}

			ofile = fopen(optarg, "w");
			if (ofile == 0) {
				perror("shuf");
				exit(1);
			}
			break;
		case 'r':
			rflag = 1;
			break;
		case '?':
			/* FALLTHROUGH */
		default:
			usage();
		}
	}

	if (iflag && eflag) {
		(void)fprintf(stderr, "shuf: cannot set the -i and -e flags at"
		    " the same time\n");
		exit(1);
	}

	if (oflag == 0) {
		ofile = stdout;
	}

	argc -= optind;
	argv += optind;

	if (argc == 0) {
		argc = 1;
		argv = def;
	}

	srand((unsigned int)time(0));

	ret = 0;
	if (eflag) {
		if (rflag) {
			randomshuffle(argv, argc);
		} else {
			shuffle(argv, argc);
		}
		goto done;
	}

	if (iflag) {
		ret = shuffle_inrange(lo, hi);
		goto done;
	}

	ret = shufflefile(argv[0]);

done:
	if (oflag) {
		(void)fclose(ofile);
	}
	return (ret);
}

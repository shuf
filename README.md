# shuf
The **shuf** utility shuffles its input by outputting a random permutation of its input lines.
It is not a one-to-one reimplementation of the `shuf(1)` utility from GNU coreutils since it lacks some of its
command-line options.

## Features
* Quality
    * Compiled with security hardening flags.
    * Static analysis integrated using clang's `scan-build` using checkers `alpha.security`, `alpha.core.CastSize`,
    `alpha.core.CastToStruct`, `alpha.core.IdenticalExpr`, `alpha.core.PointerArithm`, `alpha.core.PointerSub`,
    `alpha.core.SizeofPtr`, `alpha.core.TestAfterDivZero`, `alpha.unix`.
    * Follows [FreeBSD coding style](https://www.freebsd.org/cgi/man.cgi?query=style&sektion=9).
* Portable
    * C99 compliant *and* may be built in an environment which provides POSIX.1-2001 system interfaces.
    * Self-contained, no external dependencies.
    * Easy to compile and uses POSIX make.

## Limitations
* Input lines are limited to `BUFSIZ` bytes in lenght.
* Does not implement the `-z` and `--random-source=file` flags present in GNU coreutils' `shuf(1)`.
* Uses the `rand()` function from `stdlib.h`.

## Build dependencies
The only dependency is a toolchain supporting the following flags:

```
CFLAGS = -std=c99 -O2 -Wall -Wextra -Wpedantic \
	-Walloca -Wcast-qual -Wconversion -Wformat=2 -Wformat-security \
	-Wnull-dereference -Wstack-protector -Wvla -Warray-bounds \
	-Wbad-function-cast -Wconversion -Wshadow -Wstrict-overflow=4 -Wundef \
	-Wstrict-prototypes -Wswitch-default -Wfloat-equal -Wimplicit-fallthrough \
	-Wpointer-arith -Wswitch-enum \
	-D_FORTIFY_SOURCE=2 \
	-fstack-protector-strong -fPIE -fstack-clash-protection

LDFLAGS = -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack -Wl,-z,separate-code
```

Otherwise you can just remove the security flags and compile it with
```
CFLAGS = -std=c99 -O2 -Wall -Wextra -Wpedantic
LDFLAGS =
```

or pass your own flags to make
```sh
make CC=gcc CFLAGS=... LDFLAGS=...
```

## Installation
Clone this repository then

```sh
$ make PREFIX=/usr install
```

This will install the compiled binary under `PREFIX` (`/usr/bin`) in this case, if not specified `PREFIX` will default
to `/usr/local`. For staged installs, `DESTDIR` is also supported. As the binary does not have any dependency it does
not have to be installed before use.

## Usage
```
shuf [-h] [-n count] [-o outfile] [-r] [file]
shuf [-h] -e [-n count] [-o outfile] [-r] [args ...]
shuf [-h] -i lo-hi [-n count] [-o outfile] [-r]
```
**shuf** has three modes of operation that control how the input is obtained. By default it takes input from standard
input. The input file can be omitted, in this case the program takes the input from the standard input until `EOF` or
`^D` is reached. If a file is a single dash (‘-’), **shuf** reads from standard input. The other two modes can be
switched by means of the `-e` and `-i` flags:

* `-e` treats each command-line operand as an input line.
* `-i` acts as input came from a file containing the numbers in the interval of unsigned integers [lo,hi] one on each
line.

The three modes cannot be mixed together.

The other options are as follows:

* `-h` print usage information and exit.
* `-n count` output at most `count` lines.
* `-o outfile` output lines are printed on the specified file instead of standard output.
* `-r` repeat values. When this option is turned on, the output no longer contains permutations of the input lines,
instead each output line is randomly chosen from all the input lines. If `-n` is not also given, `shuf` prints randomly
chosen input lines indefinitely.

### Examples
Output 5 random numbers in the range 0-2:
```sh
$ shuf -n 5 -r -i 0-2
```
Flip a coin ten times:
```sh
$ shuf -e -n 10 -r Heads Tails
```
Select five numbers from 10 to 20:
```sh
$ shuf -i 10-20 -n 5
```
Shuffle what is provided on standard input and write the output to out
```sh
$ shuf -o out
```

### Static analysis
Static analysis on the code base is done by using clang's static analyzer run through `scan-build.sh` which wraps the
`scan-build` utility. The checkers used are part of the
[Experimental Checkers](https://releases.llvm.org/12.0.0/tools/clang/docs/analyzer/checkers.html#alpha-checkers)
(aka *alpha* checkers):

* `alpha.security`
* `alpha.core.CastSize`
* `alpha.core.CastToStruct`
* `alpha.core.IdenticalExpr`
* `alpha.core.PointerArithm`
* `alpha.core.PointerSub`
* `alpha.core.SizeofPtr`
* `alpha.core.TestAfterDivZero`
* `alpha.unix`

## Contributing
Send patches on the [mailing list](https://www.freelists.org/list/shuf-dev), report bugs using [git-bug](https://github.com/MichaelMure/git-bug/). 

## License
BSD 2-Clause FreeBSD License, see LICENSE.

